# Import Character Mod Guide

![Alt text](Resources/PotionBlastLogo.png)

# Introduction

This project contains the Unity project to create custom mods for the game [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/)

The documentation is split it in multiple files to keep things neat and tidy.

## Links
- [Instalation](../README.md)
- [Import Character Mod Guide](ImportCharacterModGuide.md)
- [Create Character Mod Guide](CreateCharacterModGuide.md)
- [Upload Character Mod Guide](UploadCharacterModGuide.md)

# Table of contents
1. [Introduction](#Introduction)
2. [Find Characters](#Find)
3. [Importing an existing character bundle ](#Import)

## Introduction <a name="Introduction"></a>
Custom characters are exported in a unity bundle that can be imported when the game launches.

There are some limitations and considerations to take into account. Everything is explained in detail below

## Find Characters <a name="Find"></a>
The best part of having modded characters is downloading mods made by other people.

We are currently working on the [Potion Blast Steam Workshop](https://steamcommunity.com/games/1965320) support. Once that is done you will be able to get characters from there.

In the meantime you may find characters in [discord](https://discord.gg/aNxuHwbUt3)


## Importing an existing character bundle <a name="Import"></a>
Simply download the character bundle (unzip them if necessary) and place them in the folder:

```Application.persistentDataPath+"/Resources/Mods/Characters/"```

The Application.persistentDataPath depends on your OS.
For windows this will usually be
```%userprofile%\AppData\LocalLow\Dypsloom\Potion Blast\Resources\Mods\Characters\```

If you are using an other OS please check [this page](https://docs.unity3d.com/ScriptReference/Application-persistentDataPath.html) to find where the persistentDataPath is for you.

create the folders “Resources/Mods/Characters/” if they do not already exist.

![Alt text](Resources/PotionBlastCharacterFilePathEmpty.png)


Add your custom character folder named as “<authorName>_<characterName>”.
Inside that folder you should have a folder with the name of your Operating System. ("StandaloneWindows64", “OSX”, or “Linux”)
If that character was not exported for your operating system it will not be compatible.

Finally inside you should find at least 3 files, a catalaog hash, a catalog json and the bundle file.

![Alt text](Resources/PotionBlastCharacterFilePath.png)

Now the next time you open the game you should see the character in the character select screen.
