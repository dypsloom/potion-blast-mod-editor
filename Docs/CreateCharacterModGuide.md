# Create Character Mod Guide

![Alt text](Resources/PotionBlastLogo.png)

# Introduction

This project contains the Unity project to create custom mods for the game [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/)

The documentation is split it in multiple files to keep things neat and tidy.

## Links
- [Instalation](../README.md)
- [Import Character Mod Guide](ImportCharacterModGuide.md)
- [Create Character Mod Guide](CreateCharacterModGuide.md)
- [Upload Character Mod Guide](UploadCharacterModGuide.md)

# Table of contents
1. [Introduction](#Introduction)
2. [Before Getting Started](#Before)
3. [Setting up a character and its definition](#Start)
4. [Exporting Character](#Export)
5. [Additional Customization](#More)

## Introduction <a name="Introduction"></a>

To create a custom character for  [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/) you must first [Install](../README.md) the Character Mode Editor Unity project

Once installed open the unity project to get started.

## Before Getting Started <a name="Before"></a>

Before getting started, let’s go through all the things a character requires and what is optional.
- Character Model prefab with the following components
    -  Character Model
    - Animator
- Character Model Preview prefab  with the following components
    - Character Model Preview
- Score Potion prefab  with the following components
    - Score Potion
- Character Definition ScriptableObject
    - This object groups contains all the other objects in addition to the following
    - Name
    - Author
    - Description
    - Icon
    - Color
    - Directional Rune Material

Once all of these are setup correctly they can be exported.

There are some limitations.
Scripts and shaders are not exported/imported. Therefore only scripts and shaders that exist in the original Potion Blast game can be used.
(If you wish for custom scripts/shaders, contact us on our [discord](https://discord.gg/aNxuHwbUt3) and or by email: contact@dypsloom.com so that we can add them in the build)



## Setting up a character and its definition <a name="Start"></a>
First import the model of your character.

All Characters are supported whether it is 2D, 3D humanoid or not.

The easiest to setup is 3D humanoid characters since they do not require any custom animations. The start of the setup is the same for all character types.

Open the ```PotionBlastCharacterEditorScene``` scene



If you have a humanoid character like so

![Alt text](Resources/SetModelHumanoid.png)

Make sure the Rig is set to animation type humanoid.

It is first recommended to create a prefab variant of the model with materials and the correct size.  This will avoid having to do it twice: for the playable character and the character preview.

To do this drag and drop your character in the scene. Scale it so that it more or less fits in between the Green capsule and the red rectangle in terms of height.

![Alt text](Resources/ApplePieKnight_ScaleCharacter.png)

The Red rectangle  is the hitbox the character will have in game.
The Green capsule is the recommended max height of a character.

For the material Create one with a shader that exists in the main project. We recommend to use the Lit or Unlit Universal Renderer shader. Or the MIT licensed Toon Shader: 
https://github.com/Delt06/urp-toon-shader


![Alt text](Resources/ToonShaderDELTation.png)

Any of these three toon shaders can be used.

![Alt text](Resources/ApplePieKnigh_tToonShaderExample.png)


The next step is to create the Prefab variant.
Simply drag and drop the object in the folder and press "Create Variant"

![Alt text](Resources/ApplePieKnight_CreatePrefabVariant.png)

You should end up with something like this:

![Alt text](Resources/ApplePieKnight_PrefabVariant.png)

You can now create the Potion Blast character


Open the Character Mod Window

![Alt text](Resources/PotionBlastToolsCharacterModWindow.png)

Fill in the fields.

![Alt text](Resources/ApplePieKnight_SetPrefabVariant.png)

If you do not have an icon for you character, simply take a screenshot using the button. Or Manually using a screenshot button.

![Alt text](Resources/ScreenShot.png)

The screenshot button will take a screenshot using the "Screenshot" camera (which is disabled by default). You may edit the Background using the Wall material.
You may also move the "Screenshot" camera to get a more accurate framing of your characters face.

The screenshot must be more or less square. Otherwise it will be distorted.
When using a custom icon make sure to set it as a sprite otherwise you won't be able to assign it in the field.

![Alt text](Resources/SetupSpriteIcon.png)

You can finally press the “Create Potion Blast Character” button to get all the necessary objects:
A folder will automatically be created for you in the location of you model prefab:

![Alt text](Resources/ApplePieKnight_CreatePotionBlastCharacter.png)

And voila, you have a character definition with all fields assigned that can be used in Potion Blast.

![Alt text](Resources/ApplePieKnight_CharacterDefinition.png)

From there you are free to check the other files and modify them to your liking.
For more information on each element please check the [additional](#More) documentation bellow

## Exporting Character <a name="Export"></a>
Once you are happy with your character, you can now export it. To do so you will use the Addressables package.

You can do this automatically by pressing the ```Export``` button.

The Character will be exported in the Exported folder

![Alt text](Resources/ApplePieKnight_ExportCharacter.png)

You can now grab the folder with the name ````<athour>_<character>``` and copy/paste it in the games folder to import in the game. Follow the Import instructions [here](ImportCharacterModGuide.md)



## Additional Customization <a name="More"></a>

You may want to customize your character even further to add some of the following:
- Spawn/Death Transitions
- Win/Lose Effects
- Customize the Score Potion
- Make a custom Animator Controller for your character

To do so head the the character Definition and edit the assets appropriatly.

//TODO go in more detail in each of those to show examples of how it can be done.