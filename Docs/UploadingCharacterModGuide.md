# Upload Character Mod Guide

![Alt text](Resources/PotionBlastLogo.png)

# Introduction

This project contains the Unity project to create custom mods for the game [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/)

The documentation is split it in multiple files to keep things neat and tidy.

## Links
- [Instalation](../README.md)
- [Import Character Mod Guide](ImportCharacterModGuide.md)
- [Create Character Mod Guide](CreateCharacterModGuide.md)
- [Upload Character Mod Guide](UploadCharacterModGuide.md)

# Table of contents
1. [Introduction](#Introduction)
2. [Share Characters](#Share)

## Introduction <a name="Introduction"></a>
Assuming you have created your own character using the instructions [here](CreateCharacterModGuide.md).
It is now time to share your creation with the world!

## Share Characters <a name="Share"></a>
The best part of having modded characters is sharing mods with the world.

We are currently working on the [Potion Blast Steam Workshop](https://steamcommunity.com/games/1965320) support. Once that is done you will be able to upload characters over there.

In the meantime you may add characters in a [google drive](https://drive.google.com/drive/folders/1k-7VswTVK3F85k4r2suNLO2ng4N6g0T7?usp=sharing)

Or Join the [discord](https://discord.gg/aNxuHwbUt3) to share your mods with the community.