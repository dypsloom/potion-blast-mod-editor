# Potion Blast Mod Editor

![Alt text](Docs/Resources/PotionBlastLogo.png)

# Introduction

This project contains the Unity project to create custom mods for the game [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/)

Currently [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/) only supports importing custom characters. There are plans for allowing custom zones/stages in the future.

The documentation is split it in multiple files to keep things neat and tidy.

If you are simply intrested in importing existing Characters and not make your own. Check out the [Import Character Mod Guide](Docs/ImportCharacterModGuide.md) page.

## Links
- [Instalation](README.md) 
- [Import Character Mod Guide](Docs/ImportCharacterModGuide.md)
- [Create Character Mod Guide](Docs/CreateCharacterModGuide.md)
- [Upload Character Mod Guide](Docs/UploadCharacterModGuide.md)


# Table of Contents
1. [Instalation](#introduction)
2. [Support](#Support)
3. [Roadmap](#Roadmap)
4. [License](#License)


## Installation <a name="Installation"></a>

This repo is a unity project. Follow these instructions to install it:

1. Download Unity version 2020.3.30f : 
    1. (Recommend fist getting unity hub) https://unity.com/download
    2. (Get the exact version here) https://unity.com/releases/editor/archive
2. Download/Clone this repo containing the Potion Blast Mod Editor Unity project

Once downloaded and installed open the Potion Blast Mod Editor Unity project and follow the instructions for [creating you own character ](Docs/CreateCharacterModGuide.md).


## Unity Packages

Note: The project uses a few unity packages, most notably URP 10.8.1

![Alt text](Docs/Resources/PackageManager.png)

There are other packages which are not included in the mod editor but are part of the main project such as:
https://assetstore.unity.com/packages/tools/particles-effects/boing-kit-dynamic-bouncy-bones-grass-and-more-135594
https://assetstore.unity.com/packages/vfx/shaders/toony-colors-pro-2-8105
These can be used if you own them.

You may request specific packages and or scripts to be added in the game. Simply contact us on our [discord](https://discord.gg/aNxuHwbUt3) and or by email: contact@dypsloom.com

We also use an MIT licensed Toon Shader
https://github.com/Delt06/urp-toon-shader

## Support <a name="Support"></a>
- Join our [discord](https://discord.gg/aNxuHwbUt3)
- or email us at: contact@dyplsoom.com

## Roadmap <a name="Roadmap"></a>
This depends on how well the game sells. This will define how much time we are able to spend on improving the content for [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/)

But if things go well the idea would be to implement the following

- [x] Character Mods
- [ ] Add Steam workshop support
- [ ] Zone/Stage Mods
- [ ] Variant Mods 

## License <a name="License"></a>
This project can be used as is. All assets are owned by us and must be used for the only purpose of making mods for [Potion Blast - Battle of Wizards](https://store.steampowered.com/app/1965320/Potion_Blast__Battle_of_Wizards/)





