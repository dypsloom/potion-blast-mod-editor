using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class CharacterModWindow : EditorWindow
{
    private TextField m_CharacterNameField;
    private TextField m_AuthorNameField;
    private ColorField m_ColorField;
    private ObjectField m_CharacterModelField;
    private ObjectField m_CharacterIconField;
    
    private ObjectField m_CharacterDefinitionField;
    

    private const string c_CharacterPreviewTemplateGUID = "49dcf04e7d60f6c4e87edf4b03a5da9f";
    private const string c_CharacterPreviewAnimatorControllerTemplateGUID = "a4ffdd42ae1f632429af992f350d47c4";
    private const string c_CharacterAimDirectionTemplateGUID = "408462662a6514848ad94f770a125b3d";
    private const string c_CharacterScorePotionTemplateGUID = "49dcce828392bd540b75a8236bfb5c75";
    private const string c_CharacterScorePotionLiquidTemplateGUID = "a017d9cc79bfe5f42bc737e430fb8e44";
    

    private const string c_EditorPrefKey_Name = "EditorPrefKey_Name"; 
    private const string c_EditorPrefKey_Author = "EditorPrefKey_Author"; 
    private const string c_EditorPrefKey_Color = "EditorPrefKey_Color"; 
    private const string c_EditorPrefKey_Icon = "EditorPrefKey_Icon";
    private const string c_EditorPrefKey_Prefab = "EditorPrefKey_Prefab";
    private const string c_EditorPrefKey_Definition = "EditorPrefKey_Definition";
    
    
    [MenuItem("PotionBlastTools/CharacterModWindow")]
    public static void ShowMyEditor()
    {
        // This method is called when the user selects the menu item in the Editor
        EditorWindow wnd = GetWindow<CharacterModWindow>();
        wnd.titleContent = new GUIContent("Character Mod Window");
    }
    
    public void CreateGUI()
    {
        rootVisualElement.Add(new Label("Set a model in the field to necessary objects required for Potion Blast Character"));

        //Name
        m_CharacterNameField = new TextField("Character Name");
        m_CharacterNameField.value = EditorPrefs.GetString(c_EditorPrefKey_Name,"MyCharacterName");
        m_CharacterNameField.RegisterValueChangedCallback(ctx =>
            EditorPrefs.SetString(c_EditorPrefKey_Name, ctx.newValue));
        rootVisualElement.Add(m_CharacterNameField);
        
        //Author
        m_AuthorNameField = new TextField("Author");
        m_AuthorNameField.value = EditorPrefs.GetString(c_EditorPrefKey_Author,"MySteamName");
        m_AuthorNameField.RegisterValueChangedCallback(ctx =>
            EditorPrefs.SetString(c_EditorPrefKey_Author, ctx.newValue));
        rootVisualElement.Add(m_AuthorNameField);

        var horizontalLayout = new VisualElement();
        horizontalLayout.style.flexDirection = FlexDirection.Row;
        
        //Icon
        m_CharacterIconField = new ObjectField("Character Icon");
        m_CharacterIconField.objectType = typeof(Sprite);
        m_CharacterIconField.value = AssetDatabase.LoadAssetAtPath<Sprite>(AssetDatabase.GUIDToAssetPath(EditorPrefs.GetString(c_EditorPrefKey_Icon,"")));
        m_CharacterIconField.RegisterValueChangedCallback(ctx =>
            EditorPrefs.SetString(c_EditorPrefKey_Icon, AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(ctx.newValue))));
        m_CharacterIconField.style.flexGrow = 1;
        m_CharacterIconField.style.flexShrink = 1;
        horizontalLayout.Add(m_CharacterIconField);

        var takeScreenShotButton = new Button(TakeCharacterScreenshot);
        takeScreenShotButton.text = "screenshot";
        horizontalLayout.Add(takeScreenShotButton);
        
        rootVisualElement.Add(horizontalLayout);

        //Color
        m_ColorField = new ColorField("Color");
        m_ColorField.value = Color.green;
        m_ColorField.value = StringToColor(EditorPrefs.GetString(c_EditorPrefKey_Color,"0|1|0|1"));
        m_ColorField.RegisterValueChangedCallback(ctx =>
            EditorPrefs.SetString(c_EditorPrefKey_Color, ColorToString(ctx.newValue)));
        rootVisualElement.Add(m_ColorField);

        //Character model
        var helpBox =
            new HelpBox(
                "It is recommended to specify a model prefab with the materials already set before proceeding, refer to the documentation and or video tutorial for clarifications",
                HelpBoxMessageType.Info);
        rootVisualElement.Add(helpBox);
        m_CharacterModelField = new ObjectField("Character Model");
        m_CharacterModelField.objectType = typeof(GameObject);
        m_CharacterModelField.value = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(EditorPrefs.GetString(c_EditorPrefKey_Prefab,"")));
        m_CharacterModelField.RegisterValueChangedCallback(ctx =>
            EditorPrefs.SetString(c_EditorPrefKey_Prefab, AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(ctx.newValue))));
        rootVisualElement.Add(m_CharacterModelField);

        var button = new Button(CreateCharacter);
        button.text = "Create Potion Blast character";
        rootVisualElement.Add(button);

        var space = new VisualElement();
        space.style.height = 50;
        rootVisualElement.Add(space);

        m_CharacterDefinitionField = new ObjectField("Character Definition");
        m_CharacterDefinitionField.objectType = typeof(CharacterDefinition);
        m_CharacterDefinitionField.value = AssetDatabase.LoadAssetAtPath<CharacterDefinition>(AssetDatabase.GUIDToAssetPath(EditorPrefs.GetString(c_EditorPrefKey_Definition,"")));
        m_CharacterDefinitionField.RegisterValueChangedCallback(ctx =>
            EditorPrefs.SetString(c_EditorPrefKey_Definition, AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(ctx.newValue))));
        rootVisualElement.Add(m_CharacterDefinitionField);

        var exportCharacterButton = new Button(ExportCharacterDefinition);

        exportCharacterButton.text = "Export Character";
        rootVisualElement.Add(exportCharacterButton);
    }

    private void ExportCharacterDefinition()
    {
        var characterDefinition = m_CharacterDefinitionField.value as CharacterDefinition;
        
        //Use this object to manipulate addressables
        var settings = AddressableAssetSettingsDefaultObject.Settings;

        var groups = settings.groups;
        for (int i = 0; i < groups.Count; i++) {
            var previousGroup = groups[i];
            if(settings.DefaultGroup == previousGroup){ continue; }
            settings.RemoveGroup(previousGroup);
        }

        //Create and Remove groups and labels and custom address:
        string group_name = $"Group_{characterDefinition.Author}_{characterDefinition.name}";
        string label_name = $"{characterDefinition.Author}_{characterDefinition.name}";
        string path_to_object = AssetDatabase.GetAssetPath(characterDefinition);
        string custom_address = $"{characterDefinition.Author}/{characterDefinition.name}";
        
        //Create a group with no schemas
        //settings.CreateGroup(group_name, false, false, false, new List<AddressableAssetGroupSchema> { settings.DefaultGroup.Schemas[0] });
        //Create a group with the default schemas
        
        var group = settings.FindGroup(group_name);
        if (group == null) {
            group = settings.CreateGroup(group_name, false, false, false, settings.DefaultGroup.Schemas);
        }

        //Create a Label
        //settings.AddLabel(label_name, false);

        var guid = AssetDatabase.AssetPathToGUID(path_to_object);
        //This is the function that actually makes the object addressable
        var entry = settings.CreateOrMoveEntry(guid, group);
        //entry.labels.Add(label_name);
        entry.address = custom_address;
        //You'll need these to run to save the changes!
        settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
        AssetDatabase.SaveAssets();

        var activeProfileId = settings.activeProfileId;
        var buildPathParent = $"Exported/{label_name}";
        var bundlePath = $"{buildPathParent}/{UnityEditor.EditorUserBuildSettings.activeBuildTarget}";
        var buildPath = $"{UnityEngine.Application.dataPath}/{bundlePath}/";

        //Remove previous files.
        if (Directory.Exists(buildPath)) {
            Directory.Delete(buildPath, true);
        }

        settings.profileSettings.SetValue(activeProfileId, "Local.BuildPath", buildPath);
        settings.buildSettings.bundleBuildPath = buildPath;

        AddressableAssetSettings.BuildPlayerContent();

        var defaultBuildPath = "[UnityEngine.Application.dataPath]/Exported/";
        settings.profileSettings.SetValue(activeProfileId, "Local.BuildPath", defaultBuildPath);
        settings.buildSettings.bundleBuildPath = defaultBuildPath;
        
        Debug.Log("Exported to: "+buildPath);
        // Load object
        UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath($"Assets/{buildPathParent}", typeof(UnityEngine.Object));
        
        //Finally add duplicate the screenshot so that the workshop can have an icon.
        if (characterDefinition.Sprite != null) {
            AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(characterDefinition.Sprite),
                $"Assets/{buildPathParent}/Icon.png");
        }

        // Select the object in the project folder
        Selection.activeObject = obj;
 
        // Also flash the folder yellow to highlight it
        EditorGUIUtility.PingObject(obj);
    }

    private void TakeCharacterScreenshot()
    {
        string folderPath = System.IO.Directory.GetCurrentDirectory() + "/Assets/Screenshots/";

        if (!System.IO.Directory.Exists(folderPath)) {
            System.IO.Directory.CreateDirectory(folderPath);
        }
        
        var path = folderPath + "Screenshot_Character_"+
                   System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + 
                   ".png";
        var model = m_CharacterModelField.value as GameObject;
        if (model != null) {
            var characterFolder = CreateCharacterFolder(model);
            path = characterFolder + "CharacterIcon.png";
        }

        var scrennshotCamera = FindObjectOfType<ScreenshotCamera>(true);

        ScreenshotEditor.TakeScreenshotAsSpriteDelayed(scrennshotCamera,
            path,
            ()=>
            {
                scrennshotCamera.StartingScreenshot();
                GameViewUtils.TrySetSize("ScreenshotResolution");
            },
            () =>
            {
                GameViewUtils.TrySetSize("16:9");
                scrennshotCamera.FinishedScreenshot();
                
                AssetDatabase.Refresh();
                AssetDatabase.SaveAssets();

                var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
                m_CharacterIconField.value = sprite;
                
                // Select the object in the project folder
                Selection.activeObject = sprite;
 
                // Also flash the folder yellow to highlight it
                EditorGUIUtility.PingObject(sprite);
            });
    }

    public string ColorToString(Color color)
    {
        return $"{color.r}|{color.g}|{color.b}|{color.a}";
    }

    public Color StringToColor(string str)
    {
        var split = str.Split('|');
        return new Color(float.Parse(split[0]), float.Parse(split[1]),float.Parse(split[2]),float.Parse(split[3]));
    }

    private void CreateCharacter()
    {
        var model = m_CharacterModelField.value as GameObject;
        if (model == null) {
            Debug.LogWarning("Please specify a model, in the model field");
            return;
        }

        var modelAnimator = model.GetComponent<Animator>();
        if (modelAnimator != null) {
            modelAnimator.applyRootMotion = false;
        }

        Debug.Log("Starting Character Creation Process");
        
        Debug.Log("Creating folder");
        var characterFolder = CreateCharacterFolder(model);

        Debug.Log("Creating Character Preview Prefab");
        var modelPreviewPrefab = CreateCharacterModelPreview(model, characterFolder);

        Debug.Log("Creating Character Model Prefab");
        var characterModelPrefab = CreateCharacterModel(model, characterFolder);
        
        Debug.Log("Creating Character Score Potion Prefab");
        var scorePotionPrefab = CreateCharacterScorePotion(model, characterFolder);
        
        Debug.Log("Creating Character Aim Direction Material");
        var characterAimMaterial = CreateCharacterAimDirectionMaterial(model, characterFolder);
        
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        
        Debug.Log("Creating Character Definition");
        var definitionName = String.Concat(m_CharacterNameField.value.Where(c => !Char.IsWhiteSpace(c)));
        definitionName = string.Join("_", definitionName.Split(Path.GetInvalidFileNameChars()));  
        var characterDefinition = ScriptableObject.CreateInstance<CharacterDefinition>();
        characterDefinition.name = definitionName;
        characterDefinition.Author = m_AuthorNameField.value;
        characterDefinition.SetDisplayName(m_CharacterNameField.value);
        characterDefinition.Color = m_ColorField.value;
        characterDefinition.Description = $"Character named '{m_CharacterNameField.value}' made by '{m_AuthorNameField.value}'";
        characterDefinition.Sprite = m_CharacterIconField.value as Sprite;
        characterDefinition.ModelPrefab = characterModelPrefab;
        characterDefinition.CharacterModelPreview = modelPreviewPrefab;
        characterDefinition.ScorePotion = scorePotionPrefab;
        characterDefinition.AimRuneMaterial = characterAimMaterial;
        characterDefinition.UseTemplateAnimator = true;
        characterDefinition.AddWhiteOutline = true;

        var characterDefinitionPath = characterFolder+"/"+characterDefinition.name+".asset";
        AssetDatabase.CreateAsset(characterDefinition, characterDefinitionPath);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        characterDefinition = AssetDatabase.LoadAssetAtPath<CharacterDefinition>(characterDefinitionPath);
        m_CharacterDefinitionField.value = characterDefinition;
        
        Debug.Log("Character Creation Process Finished Successfully");

        // Select the object in the project folder
        Selection.activeObject = characterDefinition;
 
        // Also flash the folder yellow to highlight it
        EditorGUIUtility.PingObject(characterDefinition);
    }

    private static string CreateCharacterFolder(GameObject model)
    {
        var modelPath = AssetDatabase.GetAssetPath(model);
        var parentPath = Path.GetDirectoryName(modelPath);
        Debug.Log(parentPath);
        var characterFolder = parentPath + "/PotionBlastCharacter";
        if (AssetDatabase.IsValidFolder(characterFolder) == false) {
            AssetDatabase.CreateFolder(parentPath, "PotionBlastCharacter");
        }

        return characterFolder;
    }

    private Material CreateCharacterAimDirectionMaterial(GameObject model, string characterFolder)
    {
        var materialPath = characterFolder + "/AimDirection.mat";
        AssetDatabase.CopyAsset(AssetDatabase.GUIDToAssetPath(c_CharacterAimDirectionTemplateGUID),
            materialPath);

        var material =
            AssetDatabase.LoadAssetAtPath<Material>(materialPath);
        
        material.SetColor("_EmissiveColor", m_ColorField.value);

        return material;
    }

    private GameObject CreateCharacterScorePotion(GameObject model, string characterFolder)
    {
        var scorePotionPath = characterFolder + "/ScorePotion.prefab";
        AssetDatabase.CopyAsset(AssetDatabase.GUIDToAssetPath(c_CharacterScorePotionTemplateGUID),
            scorePotionPath);
        
        var scorePotionLiquidMaterialPath = characterFolder + "/ScorePotionLiquid.mat";
        AssetDatabase.CopyAsset(AssetDatabase.GUIDToAssetPath(c_CharacterScorePotionLiquidTemplateGUID),
            scorePotionLiquidMaterialPath);
        
        var scorePotionLiquid =
            AssetDatabase.LoadAssetAtPath<Material>(scorePotionLiquidMaterialPath);
        var scorePotionPrefab =
            AssetDatabase.LoadAssetAtPath<GameObject>(scorePotionPath);
        
        scorePotionLiquid.SetColor("_MainTexColor", m_ColorField.value);

        var renderer = scorePotionPrefab.GetComponent<CharacterScorePotion>().rend;

        renderer.material = scorePotionLiquid;
        
        PrefabUtility.SavePrefabAsset(scorePotionPrefab);
        
        return scorePotionPrefab;
    }

    private GameObject CreateCharacterModel(GameObject model, string characterFolder)
    {
        var instance = PrefabUtility.InstantiatePrefab(model) as GameObject;
        CharacterModel characterModel = instance.GetComponent<CharacterModel>();
        if ( characterModel == null) {
            characterModel = instance.AddComponent<CharacterModel>();
        }

        var animator = characterModel.GetComponent<Animator>();
        characterModel.SkinnedMeshRenderers = instance.GetComponentsInChildren<SkinnedMeshRenderer>(true);

        characterModel.RightHand = animator.GetBoneTransform(HumanBodyBones.RightHand);
        characterModel.LeftHand = animator.GetBoneTransform(HumanBodyBones.LeftHand);

        if (characterModel.RightHand != null) {
            var backwardHandChild = new GameObject("BackwardHandChild").transform;
            backwardHandChild.SetParent(characterModel.RightHand);
            characterModel.BackwardHandChild = backwardHandChild;
        }
        
        if (characterModel.LeftHand  != null) {
            var forwardHandChild = new GameObject("ForwardHandChild").transform;
            forwardHandChild.SetParent(characterModel.LeftHand );
            characterModel.ForwardHandChild = forwardHandChild;
        }

        var path = characterFolder + "/CharacterModel.prefab";
        
        return CreatePrefab(path, instance);
    }

    private GameObject CreateCharacterModelPreview(GameObject model, string characterFolder)
    {
        //First get the Character preview template
        var characterPreviewTemplatePrefab =
            AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(c_CharacterPreviewTemplateGUID));

        var characterPreviewGoInstance = Instantiate(characterPreviewTemplatePrefab);
        var characterPreviewInstance = characterPreviewGoInstance.GetComponent<CharacterModelPreview>();
        var characterParent = characterPreviewInstance.CharacterParent;
        var modelPreviewGO = PrefabUtility.InstantiatePrefab(model,characterParent) as GameObject;
        var previewAnimator = modelPreviewGO.GetComponent<Animator>();

        characterPreviewInstance.Animator = previewAnimator;


        var characterPreviewAnimatorControllerPath = characterFolder + "/CharacterAnimatorOverrideController.asset";
        AssetDatabase.CopyAsset(AssetDatabase.GUIDToAssetPath(c_CharacterPreviewAnimatorControllerTemplateGUID),
            characterPreviewAnimatorControllerPath);

        var newAnimatorController =
            AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(characterPreviewAnimatorControllerPath);
        previewAnimator.runtimeAnimatorController = newAnimatorController;
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        var characterPreviewPath = characterFolder + "/CharacterPreview.prefab";
        
        return CreatePrefab(characterPreviewPath, characterPreviewGoInstance);
    }

    private GameObject CreatePrefab(string path, GameObject prefab)
    {
        AssetDatabase.DeleteAsset(path);
        PrefabUtility.SaveAsPrefabAsset(prefab, path);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        DestroyImmediate (prefab);
        return AssetDatabase.LoadAssetAtPath<GameObject>(path);
    }
}
