using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterModel : MonoBehaviour
{
    public SkinnedMeshRenderer[] SkinnedMeshRenderers;
    public Transform RightHand;
    public Transform LeftHand;
    public Transform ForwardHandChild;
    public Transform BackwardHandChild;
    public CharacterTransitionEffect SpawnEffect;
    public CharacterTransitionEffect DespawnEffect;
    public MonoBehaviour[] ActiveComponentsWhileAlive;
    public GameObject[] ActiveObjectsWhileAlive;
    public bool InvertZScaleOnFlip;
    public GameObject SpellCastEffect;

    public bool HasSpawnEffect { get; set; }
    public bool HasDespawnEffect { get; set; }
    public bool HasForwardHandChild { get; set; }
    public bool HasBackwardHandChild { get; set; }

    private void Awake()
    {
        if (ForwardHandChild != null) {
            HasForwardHandChild = true;
        }
        if (BackwardHandChild != null) {
            HasBackwardHandChild = true;
        }
        if (SpawnEffect != null) {
            SpawnEffect.gameObject.SetActive(false);
            HasSpawnEffect = true;
        }
        if (DespawnEffect != null) {
            DespawnEffect.gameObject.SetActive(false);
            HasDespawnEffect = true;
        }
    }

    private void Start()
    {
        EnableSpellCastEffect(false);
    }

    public void StartDespawnEffect()
    {
        if (HasSpawnEffect) {
            SpawnEffect.gameObject.SetActive(false);
            SpawnEffect.StopEffect();
        }
        if (HasDespawnEffect) {
            DespawnEffect.gameObject.SetActive(true);
            DespawnEffect.StartEffect();
        }
    }
    
    public void StartSpawnEffect()
    {
        if (HasDespawnEffect) {
            DespawnEffect.gameObject.SetActive(false);
            DespawnEffect.StopEffect();
        }
        if (HasSpawnEffect) {
            SpawnEffect.gameObject.SetActive(true);
            SpawnEffect.StartEffect();
        }
    }

    public void Flip(bool lookingRight)
    {
        if (lookingRight) {
            if (HasForwardHandChild) {
                ForwardHandChild.SetParent(RightHand,false);
            }

            if (HasBackwardHandChild) {
                BackwardHandChild.SetParent(LeftHand,false);
            }
            
        } else {
            if (HasForwardHandChild) {
                ForwardHandChild.SetParent(LeftHand,false);
            }

            if (HasBackwardHandChild) {
                BackwardHandChild.SetParent(RightHand,false);
            }
        }

        if (InvertZScaleOnFlip) {
            ForwardHandChild.localScale = Vector3.Scale(new Vector3(1,1,-1), ForwardHandChild.localScale);
            BackwardHandChild.localScale = Vector3.Scale(new Vector3(1,1,-1), BackwardHandChild.localScale);
        }
    }

    public virtual void OnReset()
    {
        EnableComponents(false, ActiveComponentsWhileAlive);
        EnableObject(false, ActiveObjectsWhileAlive);
    }

    public virtual void OnRevive()
    {
        EnableComponents(true, ActiveComponentsWhileAlive);
        EnableObject(true, ActiveObjectsWhileAlive);
    }

    private void EnableObject(bool enable, GameObject[] objects)
    {
        if (objects == null) { return; }

        for (int i = 0; i < objects.Length; i++) {
            if (objects[i] == null) { continue; }

            objects[i].SetActive(enable);
        }
    }

    private void EnableComponents(bool enable, MonoBehaviour[] components)
    {
        if (components == null) { return; }

        for (int i = 0; i < components.Length; i++) {
            if (components[i] == null) { continue; }

            components[i].enabled = enable;
        }
    }

    public virtual void EnableSpellCastEffect(bool enable)
    {
        if (SpellCastEffect == null) { return; }

        SpellCastEffect.SetActive(enable);
    }
}