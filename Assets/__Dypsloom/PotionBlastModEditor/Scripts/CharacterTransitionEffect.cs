﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CharacterTransitionEffect : MonoBehaviour
{
    [SerializeField] protected PlayableDirector m_Director;
    [SerializeField] protected TimelineAsset m_TimelineAsset;
    
    public void StartEffect()
    {
        if (m_Director == null || m_TimelineAsset == null) {
            return;
        }
        m_Director.Play(m_TimelineAsset);
    }

    public void StopEffect()
    {
        if (m_Director == null) {
            return;
        } 
        m_Director.Stop();
    }
}