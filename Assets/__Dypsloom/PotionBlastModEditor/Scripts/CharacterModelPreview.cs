﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class CharacterModelPreview : MonoBehaviour, ICharacterModelPreview
{
    [SerializeField] protected Animator m_Animator;
    [SerializeField] protected Transform m_MovableVisualParent;
    [SerializeField] protected Transform m_CharacterParent;
    [SerializeField] protected Transform m_ConfirmedTransform;
    [SerializeField] protected Transform m_UnConfirmedTransform;
    [SerializeField] protected Transform m_EndgameTransform;
    [SerializeField] protected float m_SpawnTransitionTime = 0.3f;
    [SerializeField] protected float m_ConfirmTransitionTime = 0.3f;
    [SerializeField] protected GameObject m_SelectionProps;
    [SerializeField] protected GameObject m_WinProps;
    [SerializeField] protected GameObject m_LoseProps;

    [SerializeField] protected UnityEvent m_OnConfirm;
    [SerializeField] protected UnityEvent m_OnUnConfirm;
    [SerializeField] protected UnityEvent m_OnWin;
    [SerializeField] protected UnityEvent m_OnLose;

    protected int m_ConfirmedAnimationParameter = Animator.StringToHash("Confirmed");
    protected int m_WinAnimationParameter = Animator.StringToHash("Win");
    protected int m_LoseAnimationParameter = Animator.StringToHash("Lose");
    
    public Transform CharacterParent => m_CharacterParent;
    public Animator Animator { get => m_Animator; set => m_Animator = value; }

    public void SelectCharacter()
    {
        if (m_Animator != null) {
            m_Animator.SetBool(m_ConfirmedAnimationParameter, false);
        }
        
        EnableSelectionProps(true);
        EnableLoseProps(false);
        EnableWinProps(false);
        
        m_MovableVisualParent.transform.localScale = Vector3.zero;
        m_MovableVisualParent.transform.localPosition = m_UnConfirmedTransform.localPosition;
        m_MovableVisualParent.transform.localRotation = m_UnConfirmedTransform.localRotation;
        m_MovableVisualParent.transform.DOScale(Vector3.one, m_SpawnTransitionTime).SetEase(Ease.OutBounce);
    }

    public void NoLongerReady()
    {
        if (m_Animator != null) {
            m_Animator.SetBool(m_ConfirmedAnimationParameter, false);
        }

        EnableSelectionProps(true);
        EnableLoseProps(false);
        EnableWinProps(false);
        
        m_MovableVisualParent.transform.DOLocalMove(m_UnConfirmedTransform.localPosition, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        m_MovableVisualParent.transform.DOLocalRotateQuaternion(m_UnConfirmedTransform.localRotation, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        
        m_OnUnConfirm?.Invoke();
    }

    public void ConfirmCharacterSelected()
    {
        if (m_Animator != null) {
            m_Animator.SetBool(m_ConfirmedAnimationParameter, true);
        }
        
        EnableSelectionProps(true);
        EnableLoseProps(false);
        EnableWinProps(false);

        m_MovableVisualParent.transform.DOLocalMove(m_ConfirmedTransform.localPosition, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        m_MovableVisualParent.transform.DOLocalRotateQuaternion(m_ConfirmedTransform.localRotation, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        
        m_OnConfirm?.Invoke();
    }
    
    public void EndGame_Win()
    {
        if (m_Animator != null) {
            m_Animator.SetBool(m_WinAnimationParameter, true);
        }
        
        EnableSelectionProps(false);
        EnableLoseProps(false);
        EnableWinProps(true);
        
        m_MovableVisualParent.transform.localScale = Vector3.zero;
        m_MovableVisualParent.transform.localPosition = m_EndgameTransform.localPosition;
        m_MovableVisualParent.transform.localRotation = m_EndgameTransform.localRotation;
        m_MovableVisualParent.transform.DOScale(Vector3.one, m_SpawnTransitionTime).SetEase(Ease.OutBounce);

        m_MovableVisualParent.transform.DOLocalMove(m_EndgameTransform.localPosition, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        m_MovableVisualParent.transform.DOLocalRotateQuaternion(m_EndgameTransform.localRotation, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        
        m_OnWin?.Invoke();
    }
    
    public void EndGame_Lose()
    {
        if (m_Animator != null) {
            m_Animator.SetBool(m_LoseAnimationParameter, true);
        }
        
        EnableSelectionProps(false);
        EnableLoseProps(true);
        EnableWinProps(false);
        
        m_MovableVisualParent.transform.localScale = Vector3.zero;
        m_MovableVisualParent.transform.localPosition = m_EndgameTransform.localPosition;
        m_MovableVisualParent.transform.localRotation = m_EndgameTransform.localRotation;
        m_MovableVisualParent.transform.DOScale(Vector3.one, m_SpawnTransitionTime).SetEase(Ease.OutBounce);

        m_MovableVisualParent.transform.DOLocalMove(m_EndgameTransform.localPosition, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        m_MovableVisualParent.transform.DOLocalRotateQuaternion(m_EndgameTransform.localRotation, m_ConfirmTransitionTime).SetEase(Ease.Linear);
        
        m_OnLose?.Invoke();
    }
    
    private void EnableWinProps(bool activate)
    {
        if(m_WinProps == null){ return; }
        m_WinProps.SetActive(activate);
    }

    private void EnableLoseProps(bool activate)
    {
        if(m_LoseProps == null){ return; }
        m_LoseProps.SetActive(activate);
    }

    private void EnableSelectionProps(bool activate)
    {
        if(m_SelectionProps == null){ return; }
        m_SelectionProps.SetActive(activate);
    }
}