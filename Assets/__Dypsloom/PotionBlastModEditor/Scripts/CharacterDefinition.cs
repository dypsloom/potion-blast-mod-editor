﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName ="Character Definition", menuName = "Dypsloom/Character Definition")]
public class CharacterDefinition : ScriptableObject
{
    [Tooltip("The name of the character.")]
    [SerializeField] protected string m_DisplayName;
    [Tooltip("The author name must match your Steam name if you wish to upload your asset to the Steam workshop.")]
    [SerializeField] protected string m_Author;
    [Tooltip("The description of the character when uploaded to Steam workshop")]
    [TextArea(3,6)]
    [SerializeField] protected string m_Description;
    [SerializeField] protected GameObject m_ModelPrefab;
    [FormerlySerializedAs("m_PlayerModelPreview")] 
    [SerializeField] protected GameObject m_CharacterModelPreview;
    [SerializeField] protected Color m_Color = Color.white;
    [SerializeField] protected Sprite m_Sprite;
    [SerializeField] protected Material m_AimRuneMaterial;
    [SerializeField] protected GameObject m_ScorePotion;
    [SerializeField] protected bool m_UseTemplateAnimator;
    [SerializeField] protected bool m_AddWhiteOutline;


    public string DisplayName
    {
        get
        {
            if (string.IsNullOrWhiteSpace(m_DisplayName)) {
                return name;
            }
            return m_DisplayName;
        }
    }

    public void SetDisplayName(string displayName)
    {
        m_DisplayName = displayName;
    }

    public string Author { get => m_Author; set => m_Author = value; }
    public string Description { get => m_Description; set => m_Description = value; }
    public GameObject ModelPrefab { get => m_ModelPrefab; set => m_ModelPrefab = value; }
    public GameObject CharacterModelPreview { get => m_CharacterModelPreview; set => m_CharacterModelPreview = value; }
    public Color Color { get => m_Color; set => m_Color = value; }
    public Sprite Sprite { get => m_Sprite; set => m_Sprite = value; }
    public Material AimRuneMaterial { get => m_AimRuneMaterial; set => m_AimRuneMaterial = value; }
    public GameObject ScorePotion { get => m_ScorePotion; set => m_ScorePotion = value; }
    public bool UseTemplateAnimator { get => m_UseTemplateAnimator; set => m_UseTemplateAnimator = value; }
    public bool AddWhiteOutline { get => m_AddWhiteOutline; set => m_AddWhiteOutline = value; }

    
    public bool IsModCharacter { get; set; }
    public string StatKey => name;
    
    
}