using UnityEngine;

public interface ICharacterModelPreview
{
    GameObject gameObject { get; }

    void SelectCharacter();
    
    void NoLongerReady();

    void ConfirmCharacterSelected();
    
    void EndGame_Win();
    
    void EndGame_Lose();
}