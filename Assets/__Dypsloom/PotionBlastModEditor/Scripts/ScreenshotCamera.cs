using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ScreenshotCamera : MonoBehaviour
{
    [SerializeField] protected Camera m_Camera;
    [SerializeField] protected Vector2Int m_Size;
    [SerializeField] protected GameObject[] m_EnableDuringScreenshot;
    [SerializeField] protected GameObject[] m_DisableDuringScreenshot;

    static Action s_Before;
    static Action s_After;
    
    public void FinishedScreenshot()
    {
        EnableObjects(m_EnableDuringScreenshot, false);
        EnableObjects(m_DisableDuringScreenshot, true);
    }

    public void StartingScreenshot()
    {
        EnableObjects(m_EnableDuringScreenshot, true);
        EnableObjects(m_DisableDuringScreenshot, false);
    }

    private void EnableObjects(GameObject[] objs, bool activate)
    {
        for (int i = 0; i < objs.Length; i++) {
            if(objs[i].gameObject == null){ return; }
            objs[i].SetActive(activate);
        }
    }
}